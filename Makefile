NAS = nasm -felf64 -o

main: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm dict.o lib.o words.inc colon.inc
	$(NAS) $@ $<

dict.o: dict.asm lib.o
	$(NAS) $@ $<

lib.o: lib.asm
	$(NAS) $@ $<

.PHONY: clean
clean:
	rm -f *.o main

