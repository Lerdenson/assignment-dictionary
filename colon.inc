%macro colon 2
%%some_label:
	%ifdef dic_first   
		dq dic_first
	%else
		dq 0
	%endif	;saving next node link

	%2: db %1, 0	;save null-term word
	
	%define dic_first %2	;update first element
%endmacro
