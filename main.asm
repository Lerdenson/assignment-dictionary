%define BUFFER_SIZE 256

%include "lib.inc"
extern find_word

global _start

section .rodata
%include "words.inc"

not_found_text: db "No such string in this dictionary", 0
overflow_text: db "Overflow error. Input string is too long", 0


section .text

_start:
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	mov rsi, BUFFER_SIZE

	call read_in_buffer
	test rax, rax
	jz .overflow

	mov rsi, rsp
	mov rdi dic_first

	call find_word
	add rsp, BUFFER_SIZE
	test rax, rax
	jz .failure
.success:
	mov rdi, rax
	call print_string

	call exit

.failure:
	mov rdi, not_found_text
	call print_string

	call exit

.overflow:
	add rsp, BUFFER_SIZE
	mov rdi, overflow_text

	call exit

