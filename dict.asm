global find_word

extern string_equals
extern string_length


; args
;	rsi - null-term string
;	rdi - dictionary first element
;returns
;	rax - link to the element or 0(if failed)
find_word:
.loop:
	push rdi
	push rsi
	add rdi, 8  ;go to key adress

	call string_equals ; compare keys
	test rax, rax
	jnz .found

	pop rsi
	pop rdi

	mov rdi, [rdi]		;go to the next node
	test rdi, rdi		;check if it exists
	jz .end
	jmp .loop


.found:
	pop rdi					;put string link in rdi
	call string_length		;calculate size of it

	pop rdi
	add rax, rdi			; calculating correct adress
	add rax, 9
	
	ret

.end:
	xor rax, rax
	ret
